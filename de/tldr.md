## tl;dr

- [Hardware](http://git.card10.badge.events.ccc.de/card10/hardware) - hardware repo mit docs und specs
- [Firmware](http://git.card10.badge.events.ccc.de/card10/firmware) - firmware repo
- [Firmware Docs](https://firmware.card10.badge.events.ccc.de/) - rendered version der firmware-doku
- [Assembly guide](/de/assembleyourcard10) - bau dein card10 zusammen
- [LogBook](/de/logbook) - Geschichten der Reisenden
- [Interhacktions](/de/interhacktions) - eine Anleitung, um Apps zu bauen
- [Hardware-Übersicht](/de/hardware-overview)  
- [USB-C](/de/usbc)
- [personal state](/ps)
- [app](/app)
- [zusammenbau-video](/vid)