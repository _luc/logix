---
title: Assembly Video
---

Welcome to your card10, with this short video you'll learn how to assembly your own camp badge.

All that you'll need is a Torx T6 screwdriver.

- [Video Link](https://media.ccc.de/v/camp2019-2323-card10)
- [Download Flyer](/media/assembly_flyer.pdf)
- [Text Tutorial](/en/assembleyourcard10)

<iframe width="1024" height="576" src="https://media.ccc.de/v/camp2019-2323-card10/oembed" frameborder="0" allowfullscreen></iframe>
