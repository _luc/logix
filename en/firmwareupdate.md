---
title: Current Release
---
# Currently: Eggppppplant (day 3 - 00:23)
- Firmware Download: https://card10.badge.events.ccc.de/release/card10-v1.5-Eggppppplant.zip
- [Changelog](https://git.card10.badge.events.ccc.de/card10/firmware/blob/master/CHANGELOG.md#v15-2019-08-23-0018-eggppppplant)

# Update the firmware release on your card10
* download the current firmware `.zip` file to your computer
* extract it
* put your card10 into [USB storage mode](/en/gettingstarted#usb-storage)
* copy over the files you just extracted directly into the main folder. Note: on macOS devices it is recommended to use the terminal, e.g. for the broccoli release: `cp -r ~/Downloads/card10-v1.2-broccoli/* /Volumes/CARD10/`
* eject your device (if you're doing this in the command line: don't forget the `sync` on linux)
* switch your card10 off and on again

# Previous Releases

# DaikonRadish (day 2 - 20:00)
- Firmware Download: https://card10.badge.events.ccc.de/release/card10-v1.4-DaikonRadish.zip
- [Changelog](https://git.card10.badge.events.ccc.de/card10/firmware/blob/master/CHANGELOG.md#v14-2019-08-22-1943-daikonradish)

# CCCauliflower (day 2 - 00:30)
- Firmware Download: https://card10.badge.events.ccc.de/release/card10-v1.3-cccauliflower.zip
- [Changelog](https://git.card10.badge.events.ccc.de/card10/firmware/blob/master/CHANGELOG.md#v13-2019-08-22-0012-cccauliflower)

## Broccoli (day 1 - 19:00)
- Firmware Download: https://card10.badge.events.ccc.de/release/card10-v1.2-broccoli.zip

## Asparagus
As of Day 1, 18:00
Asparagus is the firmware that is loaded on the card10s handed out on day 1 from 18:00
There are currently no previous releases



