---
title: Assemble your card10
---

Your card10 comes in a bag with all its components and a [flyer/booklet/manual](/media/assembly_flyer.pdf). You can also checkout our assembly [video-tutorial](/vid/).

The main components of your card10 are two electronic boards, the _fundamental_ board and the _harmonic_ board. 
The _fundamental_ board sits directly on the wrist band, the _harmonic_ board with the display sits on top of it.
If you are curious to learn more about the two boards, you can find a more detailed description on the [hardware overview](/en/hardware-overview/) page.
When unpacking and assembling, make sure to keep your protective foil on the display at first, until you had a look at the 
[polarizing display](/en/faq/#why-should-i-consider-keeping-the-protective-foil-on-the-display) faq section.

<img class="center" alt="harmonic board with glue dot" src="/media/assemble/IMG_20190819_143216.jpg"  width="420" height="auto" align="center">

- use the glue dot to attach the battery to the harmonic board

<img class="center" alt="battery attached to harmonic board" src="/media/assemble/IMG_20190819_143128.jpg"  width="420" height="auto" align="center"> 

- plug in the battery

<img class="center" alt="attaching the spacers" src="/media/assemble/IMG_20190819_142944.jpg"  width="420" height="auto" align="center"> 

- attach the spacers with four of the screws
- put the nylon spacer next to LED3. The nylon spacer is not symmetrical, the flat side should face the fundamental board screw. Be careful not to fasten the screw too firmly to the nylon spacer, otherwise you risk damaging the thread in the spacer and the screw will become loose.
 
<img class="center" alt="both boards placed on top of each other" src="/media/assemble/IMG_20190819_143114.jpg"  width="420" height="auto" align="center"> 

- carefully connect the two boards with the connector

<img class="center" alt="wristband with card10" src="/media/assemble/IMG_20190819_142436.jpg"  width="420" height="auto" align="center"> 

- place the wristband with its fluffy side up, put the metal bars on top of the badge and add the remaining screws
 

<img class="center" alt="wristband with card10" src="/media/assemble/IMG_20190819_142653.jpg"  width="420" height="auto" align="center"> 



## Have fun with your card10!
Now it's time to [switch on](/en/gettingstarted/#card10-navigation)
 your card10.

<img class="center" alt="happy hacking" src="/media/assemble/IMG_m6w9xw.jpg"  width="420" height="auto" align="center"> 
